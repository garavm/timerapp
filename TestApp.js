import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View, TouchableHighlight, TextInput, Vibration } from 'react-native';
var Sound = require('react-native-sound');
import Timer from './Timer'
var moment = require('moment');


var ringTone = new Sound('loudringtone.mp3', Sound.MAIN_BUNDLE, (error) => {
    if (error) {
        console.log('failed to load the sound', error);
        return;
    }
});

const handleTimerComplete = () => {
    Vibration.vibrate(10000)
    ringTone.play((success) => {
        if (success) {
            console.log('successfully finished playing');
            setTimeout(ringTone.stop(() => {
            }), 10000);
        } else {
            console.log('playback failed due to audio decoding errors');
        }
    });
    alert('The current time(HH:mm:ss) : ' + moment(new Date()).format("HH:mm:ss"))
};

export default class TestApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timerStart: false,
            totalDuration: 10000,
            timerReset: false,

        };
        this.toggleTimer = this.toggleTimer.bind(this);
        this.resetTimer = this.resetTimer.bind(this);
    }

    toggleTimer() {
        ringTone.stop(() => {
        })
        Vibration.cancel()
        this.setState({ timerStart: !this.state.timerStart, timerReset: false });
    }

    resetTimer() {
        ringTone.stop(() => {
        })
        Vibration.cancel()
        this.setState({ timerStart: false, timerReset: true });
    }

    getFormattedTime(time) {
        this.currentTime = time;
    }
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'yellow' }}>
                <View style={{ flex: 0.2}}>
                    <Text style={{ color: 'grey', fontSize: 24 }}>Timer</Text>
                </View>
                <View style={{ flex: 0.8, justifyContent: 'flex-start', alignItems: 'center', }}>

                    <Timer totalDuration={this.state.totalDuration} msecs
                        start={this.state.timerStart}
                        reset={this.state.timerReset}
                        options={options}
                        handleFinish={handleTimerComplete}
                        getTime={this.getFormattedTime} />
                    <TouchableHighlight onPress={this.toggleTimer}>
                        <Text style={{ fontSize: 30 }}>{!this.state.timerStart ? "Start" : "Stop"}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight onPress={this.resetTimer}>
                        <Text style={{ fontSize: 30 }}>Reset</Text>
                    </TouchableHighlight>
                </View>

            </View>
        );
    }
}



const options = {
    container: {
        backgroundColor: '#000',
        padding: 5,
        borderRadius: 5,
        width: 220,
    },
    text: {
        fontSize: 30,
        color: '#FFF',
        marginLeft: 7,
    }
};