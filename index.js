import { AppRegistry } from 'react-native';
import TestApp from './TestApp';

AppRegistry.registerComponent('timerApp', () => TestApp);
